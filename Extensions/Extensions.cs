﻿using System;
using System.Collections.Generic;
using System.Linq;
using ECD_Engine.Components;
using Microsoft.Xna.Framework;

namespace ECD_Engine.Extensions
{
    public static class Extensions
    {
        public static T GetComponent<T>(this List<IComponent> source)
            //since each entity can have only one component of certain type
            where T : class
        {
            return source.OfType<T>().Select(item => item).FirstOrDefault();
        }

        public static void AddEntity(this SortedList<int, List<IComponent>> source, List<IComponent> entityComponents)
            //Generates unused and unique ID number, starting at 1
        {
            var nextID = 0; //must be initialized otherwise there is no certainty that could be used in add. method
            if (source.Keys.Count != 0)
            {
                for (int i = 1; i < source.Keys.Count + 2; i++)
                {
                    if (source.Keys.All(key => key != i))
                        nextID = i;
                }
            }
            else
            {
                nextID = 1;
            }

            if (nextID == 0) // sanity check
                throw new ArgumentException("Min ID should be 1");

            source.Add(nextID, entityComponents);
        }

        public static float Distance(this Vector2 point1, Vector2 point2)
        {
            float xd = point1.X - point2.X;
            float yd = point1.Y - point2.Y;

            return (float)Math.Sqrt(xd*xd + yd*yd);
        }
    }
}
