﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using ECD_Engine.Components;
using ECD_Engine.Extensions;
using ECD_Engine.Patterns;
using ECD_Engine.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ECD_Engine
{
    public struct ScreenCords
    {
        public Point CentrePoint { get; }
        public Vector2 Max { get; }
        public Vector2 PlayerStartingPos { get; }
        public Vector2 UfoStartingPos => ServiceLocator.Random.CoinFlip() ? new Vector2(Max.X, CentrePoint.Y) : new Vector2(0, CentrePoint.Y);
        public Vector2[] BigRockStartingPos { get; private set; }
        public Vector2 LivesPanelPos => new Vector2(0,20);

        public ScreenCords(int viewportWidth, int viewportHeight)
        {
            CentrePoint = new Point(viewportWidth / 2, viewportHeight / 2);
            Max = new Vector2(viewportWidth, viewportHeight);
            PlayerStartingPos = new Vector2(CentrePoint.X, CentrePoint.Y);
            BigRockStartingPos = new[]
            {
                new Vector2(Max.X / 4, Max.Y/2),
                new Vector2(Max.X, 0),
                new Vector2(0, Max.Y/2),
                new Vector2(Max.X / 6, 0),
            };
        }


    }

    public struct Balance
    {
        //Player Properties
        public float PlayerAcceleration => 600f;
        public float PlayerRotationSpeed => 10f;
        public float PlayerAfterSpawnInvulnerability => 5f;
        public float PlayerShotTick => 100f;
        public float PlayerRestartTime => 5f;

        //Bullet Properties
        public float BulletSpeed => 300f;
        public float PlayerBulletRange => 400f;
        public float UfoBulletRange => 500f;

        //UFO Properties
        public float UfoSpeed => 100f;
        public float UfoRotationSpeed => 400f;
        public float BigUfoTurnTick => 3f;
        public float SmallUfoTurnTick => 5f;
        public float BigUfoShotTick => 2f;
        public float SmallUfoShotTick => 1f;
        public float BigUfoPointTreshold => 1300;
        public float SmallUfoPointTreshold => 3000;
        public float BigUfoRespawnTick => 12;
        public float SmallUfoRespawnTick => 23;
        public int BigUfoPointValue => 120;
        public int SmallUfoPointValue => 150;

        //BigRockProperties
        public float BigRockSpeed => 70f;
        public float BigRockRotationSpeed => .2f;
        public int BigRockPointValue => 50;
   
        //Universal RockProprieties 
        public double RockFixedAngle => ServiceLocator.Random.RandomDouble(MathHelper.Pi / 3, 2 * MathHelper.Pi / 3);
        public float RockFixedX => (float)Math.Cos(RockFixedAngle);
        public float RockFixedY => (float)Math.Sin(RockFixedAngle);

        //MediumRockProperties
        public float MediumRockSpeed => 120f;
        public float MediumRockRotationSpeed => .25f;
        public int MediumRockPointValue => 80;

        //SmallRockProperties
        public float SmallRockSpeed => 150f;
        public float SmallRockRotationSpeed => .3f;
        public int SmallRockPointValue => 100;

    }

    public class ComponentAssembler : Singleton<ComponentAssembler>
    {
        public Dictionary<string, Texture2D> SpriteTextures { private get; set; }
        public Dictionary<string, SpriteFont> SpriteFonts {private get; set; }
        public Dictionary<AnimState, Texture2D> StateTextures { get; set; }
        public ScreenCords ScreenCords { private get; set; }
        private Balance balance = new Balance(); 
        

        public List<IComponent> AssembleLargeRock(float posX, float posY)
        {
            var list = new List<IComponent>
            {
                new DrawAble {IsActive = true, Texture = SpriteTextures["BigRock"]},
                new BoxCollider {DebugTexture = SpriteTextures["Rect"]},
                new Position(),
                new MoveAble(),
                new Tag("BigRock"),
                new DivideAble(),
                new ScoreWorth()
            };

            list.GetComponent<ScoreWorth>().PointValue = balance.BigRockPointValue;

            list.GetComponent<Position>().PosX = posX;
            list.GetComponent<Position>().PosY = posY;

            list.GetComponent<MoveAble>().IsFLoating = true; 
            list.GetComponent<MoveAble>().IsActive = true;
            list.GetComponent<MoveAble>().RotationSpeed = balance.BigRockRotationSpeed;
            list.GetComponent<MoveAble>().VelX += balance.RockFixedX*balance.BigRockSpeed;
            list.GetComponent<MoveAble>().VelY += balance.RockFixedY*balance.BigRockSpeed;

            list.GetComponent<DivideAble>().DivisionInfo = DivideType.Big;
            list.GetComponent<BoxCollider>().DebugTexture = SpriteTextures["Rect"];
            list.GetComponent<BoxCollider>().Size = new Point(SpriteTextures[list.GetComponent<Tag>().Name].Width, SpriteTextures[list.GetComponent<Tag>().Name].Height);
            list.GetComponent<BoxCollider>().IsCollidingFilter =
                (box1, box2) => box1.BoundingBox.Intersects(box2.BoundingBox) && box2.IsActive;

            return list;
        }

        public List<IComponent> AssembleBullet(List<IComponent> components, int entityID)
        {
            var list = new List<IComponent> { new DrawAble(), new BoxCollider(), new Position(), new MoveAble(), new Tag("Bullet"), new LimitedTravelDistance()};
            var shooterPos = components.GetComponent<Position>();
            var shooterBox = components.GetComponent<BoxCollider>();

            list.GetComponent<DrawAble>().IsActive = true;
            list.GetComponent<DrawAble>().Texture = SpriteTextures[list.GetComponent<Tag>().Name];

            list.GetComponent<Position>().RadianOrientation = shooterPos.RadianOrientation;

            list.GetComponent<MoveAble>().IsActive = true;
            list.GetComponent<MoveAble>().IsFLoating = true;
            list.GetComponent<MoveAble>().VelX += list.GetComponent<Position>().DirectionX * balance.BulletSpeed;
            list.GetComponent<MoveAble>().VelY += list.GetComponent<Position>().DirectionY * balance.BulletSpeed;

            list.GetComponent<LimitedTravelDistance>().StartingPosX = shooterPos.PosX;
            list.GetComponent<LimitedTravelDistance>().StartingPosY = shooterPos.PosY;
            list.GetComponent<LimitedTravelDistance>().MaxDistance = entityID == 1
                ? balance.PlayerBulletRange
                : balance.UfoBulletRange;

            list.GetComponent<Tag>().BindedID = entityID;


            list.GetComponent<BoxCollider>().Size = new Point(SpriteTextures[list.GetComponent<Tag>().Name].Width, SpriteTextures[list.GetComponent<Tag>().Name].Height);
            list.GetComponent<BoxCollider>().IsActive = true;
            list.GetComponent<BoxCollider>().DebugTexture = SpriteTextures["Rect"];
            list.GetComponent<BoxCollider>().IsCollidingFilter =
                (box1, box2) => box1.BoundingBox.Intersects(box2.BoundingBox) && box1 != box2 && box2 != shooterBox;

            list.GetComponent<Position>().PosX = shooterBox.BoundingBox.Center.ToVector2().X + //monstrosity! tha aBomination! this WILL be refactored, so help me god
                                                 shooterBox.OriginPoint.ToVector2()
                                                     .Distance(shooterBox.BoundingBox.Center.ToVector2())*
                                                 shooterPos.DirectionX;
            list.GetComponent<Position>().PosY = shooterBox.BoundingBox.Center.ToVector2().Y + //monstrosity! tha aBomination! this WILL be refactored, so help me god
                                                 shooterBox.OriginPoint.ToVector2()
                                                     .Distance(shooterBox.BoundingBox.Center.ToVector2()) *
                                                 shooterPos.DirectionY;

            return list;
        }

        public IEnumerable<List<IComponent>> AssembleTwoMediumRocks(List<IComponent> parentRock, List<IComponent> destructorBullet)
        {
            var bulletPosition = destructorBullet.GetComponent<Position>();

            for (int i = 0; i < 2; i++)
            {
                var list = new List<IComponent>
                {
                    new DrawAble(),
                    new BoxCollider(),
                    new Position(),
                    new MoveAble(),
                    new Tag("MediumRock"),
                    new DivideAble(),
                    new ScoreWorth()
                };

                list.GetComponent<ScoreWorth>().PointValue = balance.MediumRockPointValue;

                list.GetComponent<DrawAble>().IsActive = true;
                list.GetComponent<DrawAble>().Texture = SpriteTextures[list.GetComponent<Tag>().Name];

                list.GetComponent<Position>().PosX = parentRock.GetComponent<Position>().PosX;
                list.GetComponent<Position>().PosY = parentRock.GetComponent<Position>().PosY;

                if (i == 0)
                {
                    list.GetComponent<MoveAble>().VelX += bulletPosition.DirectionX *
                                                          balance.MediumRockSpeed;
                     list.GetComponent<MoveAble>().VelY += bulletPosition.DirectionY *
                                                          balance.MediumRockSpeed;

                }
                else
                {
                    list.GetComponent<MoveAble>().VelX += bulletPosition.DirectionX + (float)Math.Cos(MathHelper.Pi/6) *
                                                          balance.MediumRockSpeed;
                    list.GetComponent<MoveAble>().VelY += bulletPosition.DirectionY + (float)Math.Sin(MathHelper.Pi/6) *
                                                          balance.MediumRockSpeed;
                }

                list.GetComponent<MoveAble>().IsFLoating = true;
                list.GetComponent<MoveAble>().IsActive = true;
                list.GetComponent<MoveAble>().RotationSpeed = balance.MediumRockRotationSpeed;
                

                list.GetComponent<DivideAble>().DivisionInfo = DivideType.Medium;
                list.GetComponent<BoxCollider>().DebugTexture = SpriteTextures["Rect"];
                list.GetComponent<BoxCollider>().Size = new Point(SpriteTextures[list.GetComponent<Tag>().Name].Width,
                    SpriteTextures[list.GetComponent<Tag>().Name].Height);
                list.GetComponent<BoxCollider>().IsCollidingFilter =
                    (box1, box2) => box1.BoundingBox.Intersects(box2.BoundingBox) && box2.IsActive;

                yield return list;
            }
        }

        public IEnumerable<List<IComponent>> AssembleTwoSmallRocks(List<IComponent> parentRock, List<IComponent> destructorBullet)
        {
            var bulletPosition = destructorBullet.GetComponent<Position>();

            for (int i = 0; i < 2; i++)
            {
                var list = new List<IComponent>
                {
                    new DrawAble(),
                    new BoxCollider(),
                    new Position(),
                    new MoveAble(),
                    new Tag("SmallRock"),
                    new DivideAble(),
                    new ScoreWorth()
                };

                list.GetComponent<ScoreWorth>().PointValue = balance.SmallRockPointValue;

                list.GetComponent<DrawAble>().IsActive = true;
                list.GetComponent<DrawAble>().Texture = SpriteTextures[list.GetComponent<Tag>().Name];

                list.GetComponent<Position>().PosX = parentRock.GetComponent<Position>().PosX;
                list.GetComponent<Position>().PosY = parentRock.GetComponent<Position>().PosY;

                if (i == 0)
                {
                    list.GetComponent<MoveAble>().VelX += bulletPosition.DirectionX *
                                                          balance.SmallRockSpeed;
                    list.GetComponent<MoveAble>().VelY += bulletPosition.DirectionY *
                                                         balance.SmallRockSpeed;

                }
                else
                {
                    list.GetComponent<MoveAble>().VelX += bulletPosition.DirectionX + (float)Math.Cos(MathHelper.Pi / 6) *
                                                          balance.SmallRockSpeed;
                    list.GetComponent<MoveAble>().VelY += bulletPosition.DirectionY + (float)Math.Sin(MathHelper.Pi / 6) *
                                                          balance.SmallRockSpeed;
                }

                list.GetComponent<MoveAble>().IsFLoating = true;
                list.GetComponent<MoveAble>().IsActive = true;
                list.GetComponent<MoveAble>().RotationSpeed = balance.SmallRockRotationSpeed;


                list.GetComponent<DivideAble>().DivisionInfo = DivideType.NoDivide;
                list.GetComponent<BoxCollider>().DebugTexture = SpriteTextures["Rect"];
                list.GetComponent<BoxCollider>().Size = new Point(SpriteTextures[list.GetComponent<Tag>().Name].Width,
                    SpriteTextures[list.GetComponent<Tag>().Name].Height);
                list.GetComponent<BoxCollider>().IsCollidingFilter =
                    (box1, box2) => box1.BoundingBox.Intersects(box2.BoundingBox) && box2.IsActive;

                yield return list;
            }
        }

        public List<IComponent> AssemblePlayer()
        {
            var list = new List<IComponent>
            {
                new DrawAble(),
                new BoxCollider(),
                new Position(),
                new MoveAble(),
                new Tag("Player"),
                new Gun(),
                new Player(),
                new StateTextures()
            };

            list.GetComponent<DrawAble>().IsActive = true;
            list.GetComponent<DrawAble>().Texture = SpriteTextures[list.GetComponent<Tag>().Name];

            list.GetComponent<Position>().PosX = ScreenCords.PlayerStartingPos.X;
            list.GetComponent<Position>().PosY = ScreenCords.PlayerStartingPos.Y ;

            list.GetComponent<MoveAble>().IsActive = true;
            list.GetComponent<MoveAble>().Acceleration = balance.PlayerAcceleration;
            list.GetComponent<MoveAble>().RotationSpeed = balance.PlayerRotationSpeed;

            list.GetComponent<Player>().Lives = 3;

            list.GetComponent<BoxCollider>().DebugTexture = SpriteTextures["Rect"];
            list.GetComponent<BoxCollider>().Size = new Point(SpriteTextures[list.GetComponent<Tag>().Name].Width, SpriteTextures[list.GetComponent<Tag>().Name].Height);
            list.GetComponent<BoxCollider>().IsActive = true;
            list.GetComponent<BoxCollider>().IsCollidingFilter =
                (box1, box2) => box1.BoundingBox.Intersects(box2.BoundingBox) && box2.IsActive && box1 != box2;

            return list;
        }

        public List<IComponent> AssembleBigUFO()
        {
            var list = new List<IComponent> { new DrawAble(), new BoxCollider(), new Position(), new MoveAble(), new Tag("BigUFO"), new Gun(), new AiControlled(), new ScoreWorth() };

            list.GetComponent<ScoreWorth>().PointValue = balance.BigUfoPointValue;

            list.GetComponent<DrawAble>().IsActive = true;
            list.GetComponent<DrawAble>().Texture = SpriteTextures[list.GetComponent<Tag>().Name];

            var startingPos = ScreenCords.UfoStartingPos;
            list.GetComponent<Position>().PosX = startingPos.X;
            list.GetComponent<Position>().PosY = startingPos.Y;
            list.GetComponent<Position>().RadianOrientation = ServiceLocator.Random.CoinFlip()
                ? MathHelper.Pi
                : -MathHelper.Pi;
   

            list.GetComponent<MoveAble>().IsActive = true;
            list.GetComponent<MoveAble>().IsFLoating = true;
            list.GetComponent<MoveAble>().RotationSpeed = balance.UfoRotationSpeed;
            list.GetComponent<MoveAble>().VelX += balance.UfoSpeed * list.GetComponent<Position>().DirectionX;
            list.GetComponent<MoveAble>().VelY += balance.UfoSpeed * list.GetComponent<Position>().DirectionY;

            list.GetComponent<BoxCollider>().Size = new Point(SpriteTextures[list.GetComponent<Tag>().Name].Height, SpriteTextures[list.GetComponent<Tag>().Name].Width);
            list.GetComponent<BoxCollider>().IsActive = true;

            list.GetComponent<AiControlled>().DirectionTick = balance.BigUfoTurnTick;
            list.GetComponent<AiControlled>().ShotTick = balance.BigUfoShotTick;
            list.GetComponent<AiControlled>().PointTreshold = balance.BigUfoPointTreshold;
            list.GetComponent<AiControlled>().RespawnTick = balance.BigUfoRespawnTick;

            list.GetComponent<BoxCollider>().DebugTexture = SpriteTextures["Rect"];
            list.GetComponent<Gun>().MarksmanshipLevel = 1;
            list.GetComponent<BoxCollider>().IsCollidingFilter =
                (box1, box2) => box1.BoundingBox.Intersects(box2.BoundingBox) && box2.IsActive && box1 != box2;

            return list;
        }

        private List<IComponent> createSmallUFO()
        {
            return new List<IComponent> { new DrawAble(), new BoxCollider(), new Position(), new MoveAble(), new Tag("SmallUFO"), new Gun(), new AiControlled() };
        }

        public List<IComponent> AssembleScoreGUILabel()
        {
            return new List<IComponent>
            {
                new DrawAble {IsActive = true, Font = SpriteFonts["ScoreLabel"], DrawOrder = DrawOrder.FirstLayer},
                new Label {Text = "0"},
                new Tag("ScoreLabel") {BindedID = 1},
                new Position {PosX = 0, PosY = 0}
            };
        }

        public List<IComponent> AssembleLivesGUIPanel()
        {
            return new List<IComponent>()
            {
                new DrawAble
                {
                    IsActive = true,
                    Texture = SpriteTextures["LivesPanel"],
                    DrawOrder = DrawOrder.FirstLayer
                },
                new Tag("LivesPanel") {BindedID = 1},
                new Position {PosX = ScreenCords.LivesPanelPos.X, PosY = ScreenCords.LivesPanelPos.Y},
                new StateTextures()
            };
        }

        public List<IComponent> AssembleShield(ref Position playerPos)
        {
            return new List<IComponent>
            {
                new DrawAble {DrawOrder = DrawOrder.SecondLayer, IsActive = true, Texture = SpriteTextures["Shield"]},
                new BoxCollider
                {
                    DebugTexture = SpriteTextures["Rect"],
                    IsActive = true,
                    IsCollidingFilter = (box1, box2) => box1.BoundingBox.Intersects(box2.BoundingBox)
                },
                new Position {PosX = playerPos.PosX, PosY = playerPos.PosX, RadianOrientation = playerPos.RadianOrientation},
                new LimitedPresence {LifeSpan = balance.PlayerRestartTime},
                new Tag("Shield") {BindedID = 1}
            };
        }
    }
}
