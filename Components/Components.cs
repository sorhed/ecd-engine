﻿using System;
using System.Collections.Generic;
using ECD_Engine.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ECD_Engine.Components
{
    public enum Message
    {
        RockDied,
        XmaXTransition,
        XminTransition,
        YmaxTransition,
        YminTransition,
        ShotFired,
        ShipDestroyed,
        RotateRight,
        RotateLeft,
        Thrust,
        EntityKilled,
        EntityAdded,
        UfoTurn,
        StartingRocksCreated,
        PlayerFraged,
        GameOver
    }

    public enum GunState
    {
        Firing,
        Resting
    }

    public enum AnimState
    {
        Straight,
        TurningLeft,
        TurningRight,
        Thrusting,
        Destroyed = 0,
        OneLive = 1,
        TwoLives = 2,
        ThreeLives = 3
    }

    public enum DrawOrder
    {
        FirstLayer = 1,
        SecondLayer = 2,
        ThirdLayer = 3,
        Background = 4
    }

    public enum DivideType
    {
        Big,
        Medium,
        NoDivide
    }

    public interface IComponent
    {
    }

    public sealed class Position : IComponent
    {
        public float PosX { get; set; }
        public float PosY { get; set; }
        public double RadianOrientation { get; set; } = -MathHelper.Pi / 2;
        public float DirectionX => (float)Math.Cos(RadianOrientation);
        public float DirectionY => (float)Math.Sin(RadianOrientation);
        public Vector2 PosVector => new Vector2((int)PosX, (int)PosY);
        public Vector2 DirectionVector => new Vector2(DirectionX, DirectionY);
    }

    public sealed class BoxCollider : IComponent //do zmiany na sfere
    {

        public bool IsActive { get; set; } //if is not active then it serves only as a passive collision target (can not collide but can be collided with)
        public Point Size { get; set; }
        public Point OriginPoint { get; set; }
        public Func<BoxCollider, BoxCollider, bool> IsCollidingFilter { get; set; }
        public Rectangle BoundingBox => new Rectangle(OriginPoint, Size);
        public Texture2D DebugTexture { get; set; }
    }

    public sealed class DrawAble : IComponent
    {
        public Texture2D Texture { get; set; }
        public Color Color { get; set; }
        public bool IsActive { get; set; }
        public DrawOrder DrawOrder { get; set; }
        public SpriteFont Font { get; set; }
    }

    public sealed class Label : IComponent
    {
        public string Text { get; set; }
    }

    public sealed class MoveAble : IComponent
    {
        public bool IsFLoating { get; set; }
        public bool IsActive { get; set; }
        public float Acceleration { get; set; }
        public float RotationSpeed { get; set; }
        public float VelX { get; set; }
        public float VelY { get; set; }

        public Vector2 Velocity => new Vector2(VelX, VelY);
    }

    public sealed class AiControlled : IComponent
    { 
        public float DirectionTick { get; set; }
        public float ShotTick { get; set; }
        public float PointTreshold { get; set; }
        public float RespawnTick { get; set; }
    }

    public sealed class Tag : IComponent
    { 
        public string Name { get; private set; }
        public int BindedID { get; set; } //used when in need of binding one entity onto another for some reason (like info on whom bullet destroyed given entity)

        public Tag (string name)
        {
            Name = name;
        }
    }

    public sealed class Gun : IComponent
    {
        public bool IsActive { get; set; }
        public int MarksmanshipLevel { get; set; }
        public GunState GunState { get; set; } = GunState.Resting;

    }

    public sealed class Player : IComponent
    {
        public int Lives { get; set; }
        public int Score { get; set; }
    }

    public sealed class DivideAble : IComponent
    {
        public DivideType DivisionInfo { get; set; }
    }

    public sealed class ScoreWorth : IComponent
    {
        public int PointValue { get; set; }  
    }

    public sealed class StateTextures : IComponent
    {
        public SortedDictionary<AnimState, Texture2D> Animations { get; set; }
        public AnimState AnimState { get; set; }
    }

    public sealed class LimitedTravelDistance : IComponent
    {
        public float DistanceTraveled { get; set; }
        public float MaxDistance { get; set; }
        public float StartingPosX { get; set; }
        public float StartingPosY { get; set; }
        public Vector2 StartingPos => new Vector2(StartingPosX, StartingPosY);
    }

    public sealed class LimitedPresence : IComponent
    {
        public float LifeSpan { get; set; }
    }

}

