﻿using System.Collections.Generic;
using System.Threading;
using ECD_Engine.Components;
using ECD_Engine.InputHandling;
using ECD_Engine.Patterns;
using ECD_Engine.Services;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ECD_Engine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        KeyboardEventManager eventSender;
        readonly InputHandler handler = new InputHandler();
        readonly RandomNumbersService randomSys = new RandomNumbersService();
        private GameManager gameManager;
        private Dictionary<string, Texture2D> spriteTextures = new Dictionary<string, Texture2D>();
        private Dictionary<string, SpriteFont> spriteFonts = new Dictionary<string, SpriteFont>();
        private Dictionary<AnimState, Texture2D> stateTextures = new Dictionary<AnimState, Texture2D>();


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            eventSender = new KeyboardEventManager(this);
            gameManager = new GameManager(this);
            ServiceLocator.Command = handler;
            ServiceLocator.Random = randomSys;

            Components.Add(eventSender);
            Components.Add(gameManager);
            eventSender.KeyPressed += handler.HandleInput;
            eventSender.KeysRealeased += handler.HandleOnRelease;


            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //creating plain texture for drawing BoxColliders
            Texture2D whiteRectangle = new Texture2D(GraphicsDevice, 1, 1);
            whiteRectangle.SetData(new[] { Color.White });

            //Populating textures list
            spriteTextures.Add("Player", Content.Load<Texture2D>("PlayerModel/player"));
            spriteTextures.Add("Bullet", Content.Load<Texture2D>("PlayerModel/laserGreen"));
            spriteTextures.Add("BigUFO", Content.Load<Texture2D>("EnemyModel/enemyUfo"));
            spriteTextures.Add("BigRock", Content.Load<Texture2D>("RockModels/BigRock"));
            spriteTextures.Add("MediumRock", Content.Load<Texture2D>("RockModels/MedmiumRock"));
            spriteTextures.Add("SmallRock", Content.Load<Texture2D>("RockModels/SmallRock"));
            spriteTextures.Add("Shield", Content.Load<Texture2D>("PlayerModel/shield"));
            spriteTextures.Add("Rect", whiteRectangle);

            //Populating SpriteFonts list
            spriteFonts.Add("ScoreLabel", Content.Load<SpriteFont>("SpriteFonts/Arcade"));

            //Populating StateTextures list || needs rethinking; class or dictionary inside a dictionary ;x
            stateTextures.Add((AnimState)1, Content.Load<Texture2D>("PlayerModel/ThreeLives"));
            stateTextures.Add((AnimState)2, Content.Load<Texture2D>("PlayerModel/TwoLives"));
            stateTextures.Add((AnimState)3, Content.Load<Texture2D>("PlayerModel/life"));

            //Passing references to the entityfactory
            ComponentAssembler.Instance.SpriteTextures = spriteTextures;
            ComponentAssembler.Instance.SpriteFonts = spriteFonts;
            ComponentAssembler.Instance.StateTextures = stateTextures;

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            spriteTextures["Rect"].Dispose();
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            gameManager.Update(gameTime);

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            gameManager.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
