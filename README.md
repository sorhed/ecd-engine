# ECD-eng
Asteroids using entities - component - system design.

By doing this project I am hoping to achieve better understanding of more data-oriented application design as well as the ECS itself.

The project is carried out in MonoGame without any additional libraries; generally I treat it as stepping stone to more advanced installments. I am also trying out various design patterns; the observer is bit odd though; I have not used the native events. After gameplay is roughly finished I am planning to refactor it along with the needles iterations in the systems update cycles. Also, I am planning to implement particle effect system in order to cover explosions and other effects like them.