﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using ECD_Engine.Components;
using ECD_Engine.Extensions;
using ECD_Engine.InputHandling;
using ECD_Engine.Patterns;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ECD_Engine.Systems;
using Microsoft.Xna.Framework.Content;

namespace ECD_Engine
{
    class GameManager : DrawableGameComponent, IObserver
    {
        private Subject subject = new Subject();
        private readonly ScreenCords screenCords;
        private SubSystem[] subSystems;
        private readonly SortedList<int, List<IComponent>> entitiesSortedList = new SortedList<int, List<IComponent>>();


        public GameManager(Game game) : base(game)
        {
            Subject.Instance.AddObserver(this);
            screenCords = new ScreenCords(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
        }

        public override void Initialize()
        {
            subSystems = new SubSystem[8]
            {
                new GameLogicSubSystem(entitiesSortedList, screenCords),
                new ManualControlSubSystem(entitiesSortedList, screenCords),
                new MoveSubSystem(entitiesSortedList, screenCords),
                new CollisionSubSystem(entitiesSortedList, screenCords),
                new GunControlSubSystem(entitiesSortedList, screenCords),
                new LimitedPresenceSubSystem(entitiesSortedList, screenCords),
                new AiSubSystem(entitiesSortedList, screenCords), 
                new DrawSubSystem(entitiesSortedList, screenCords),
            };
        
            ComponentAssembler.Instance.ScreenCords = screenCords;

        }

        public override void Update(GameTime gameTime)
        {
            float fDeltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (fDeltaTime > 0.1f)
                fDeltaTime = 0.1f;

            foreach (var sys in subSystems)
            {
                sys.Update(fDeltaTime);
            }

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var sys in subSystems)
            {
                sys.Draw(spriteBatch);
            }
        }

        public void OnNotify(Message message)
        {
            if (message == Message.StartingRocksCreated)
            {
                for (int i = 0; i < 4; i++)
                {
                    entitiesSortedList.AddEntity(ComponentAssembler.Instance.AssembleLargeRock(screenCords.BigRockStartingPos[i].X, screenCords.BigRockStartingPos[i].Y));
                }
            }
            if (message == Message.GameOver)
            {
                //GameOver logic
            }
        }

        public void OnNotify<T>(Message message, T eventData)
        {
            if (message == Message.EntityKilled && eventData is int)
            {
                var edata = Convert.ToInt32(eventData);
                if (edata == 1)
                {
                    Subject.Instance.Notify(Message.ShipDestroyed);
                }
                else
                {
                    entitiesSortedList.Remove(edata);
                }
                
            }
            if (message == Message.EntityAdded && eventData is List<IComponent>)
            {
                entitiesSortedList.AddEntity(eventData as List<IComponent>);
            }
        }

        public void OnNotify<T, Y>(Message message, T eventData, Y eventData2)
        {
            if (message == Message.EntityAdded && eventData2 is bool && eventData is CollisionSubSystem.ColliderPair)
            {
                var pair = eventData as CollisionSubSystem.ColliderPair;
                foreach ( var entity in Convert.ToBoolean(eventData2)
                            ? ComponentAssembler.Instance.AssembleTwoMediumRocks(entitiesSortedList[pair.DestroyedID], entitiesSortedList[pair.DestructorID])
                            : ComponentAssembler.Instance.AssembleTwoSmallRocks(entitiesSortedList[pair.DestroyedID], entitiesSortedList[pair.DestructorID])) 
                {
                    entitiesSortedList.AddEntity(entity);
                }
            }
        }
    }
}
