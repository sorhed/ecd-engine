﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ECD_Engine.Components;
using ECD_Engine.Extensions;
using ECD_Engine.Patterns;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ECD_Engine.Systems
{
    public abstract class SubSystem : IObserver
    {
        protected SortedList<int, List<IComponent>> EntitiesSortedList;
        protected ScreenCords ScreenCords;
        protected Balance Balance = new Balance();
        protected float DeltaTime;

        protected SubSystem (SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
        {
            EntitiesSortedList = entitiesSortedList;
            ScreenCords = screenCords;
            Subject.Instance.AddObserver(this);
        }

        public abstract void Update(float deltaTime);

        public virtual void Draw(SpriteBatch spriteBatch)
        {
        }
        public virtual void OnNotify(Message message)
        {
        }
        public virtual void OnNotify<T>(Message message, T eventData) 
        {         
        }
        public virtual void OnNotify<T, Y>(Message message, T eventData, Y eventData2) 
        {
        }
    }

    public class DrawSubSystem : SubSystem
    {
        private Dictionary<int, double> angles;
        private Dictionary<int, Vector2> positionVectors;
        private Dictionary<int, Texture2D> textures;
        private Dictionary<int, SpriteFont> spriteFonts;
        private Dictionary<int, string> labels;

        public DrawSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
            : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {
          angles = new Dictionary<int, double>();
          positionVectors = new Dictionary<int, Vector2>();
          textures = new Dictionary<int, Texture2D>();
          spriteFonts = new Dictionary<int, SpriteFont>();
          labels = new Dictionary<int, string>();

            foreach (var entity in EntitiesSortedList)
            {
                if (entity.Value.GetComponent<DrawAble>() != null &&
                    entity.Value.GetComponent<Position>() != null)
                {
                    positionVectors.Add(entity.Key, entity.Value.GetComponent<Position>().PosVector);
                    angles.Add(entity.Key, entity.Value.GetComponent<Position>().RadianOrientation);

                    if (entity.Value.GetComponent<DrawAble>().Texture != null)
                        textures.Add(entity.Key, entity.Value.GetComponent<DrawAble>().Texture);

                    if (entity.Value.GetComponent<DrawAble>().Font != null &&
                        entity.Value.GetComponent<Label>() != null)
                    {
                        spriteFonts.Add(entity.Key, entity.Value.GetComponent<DrawAble>().Font);
                        labels.Add(entity.Key, entity.Value.GetComponent<Label>().Text);
                    }
                }

            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (var key in EntitiesSortedList.Keys.OrderBy(k => EntitiesSortedList[k].GetComponent<DrawAble>()?.DrawOrder))
            {
                if (EntitiesSortedList[key].GetComponent<DrawAble>().IsActive)
                {
                    //BoxCollider drawing for debug purposes
                    var box = EntitiesSortedList[key].GetComponent<BoxCollider>();
                    if (box != null) spriteBatch.Draw(box.DebugTexture, box.BoundingBox, Color.Crimson);

                    if (EntitiesSortedList[key].GetComponent<DrawAble>().Texture != null)
                    {
                        spriteBatch.Draw(textures[key],
                            new Rectangle((int) positionVectors[key].X, (int) positionVectors[key].Y,
                                textures[key].Width, textures[key].Height), null,
                            Color.White, (float) angles[key],
                            new Vector2(textures[key].Width/2f, textures[key].Height/2f),
                            SpriteEffects.None, 0f);
                    }

                    if (EntitiesSortedList[key].GetComponent<DrawAble>().Font != null)
                    {
                        spriteBatch.DrawString(spriteFonts[key], labels[key], positionVectors[key], Color.HotPink);
                    }
                }
            }
        }
    }

    public class MoveSubSystem : SubSystem
    {

        public MoveSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
            : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {
            foreach (var entity in EntitiesSortedList)
            {
                if (entity.Value.GetComponent<MoveAble>() != null &&
                    entity.Value.GetComponent<Position>() != null &&
                    entity.Value.GetComponent<MoveAble>().IsActive)
                {
                    entity.Value.GetComponent<Position>().PosX +=
                        entity.Value.GetComponent<MoveAble>().VelX*deltaTime;
                    entity.Value.GetComponent<Position>().PosY +=
                        entity.Value.GetComponent<MoveAble>().VelY*deltaTime;

                    if (!entity.Value.GetComponent<MoveAble>().IsFLoating)
                        //determines whether the velocity of a given entity is to be reduced over time
                    {
                        entity.Value.GetComponent<MoveAble>().VelX *= .99f;
                        entity.Value.GetComponent<MoveAble>().VelY *= .99f;
                    }
                       //Collision with screen edge triggers
                    if (entity.Value.GetComponent<Position>().PosX > ScreenCords.Max.X)
                        Subject.Instance.Notify(Message.XmaXTransition, entity.Key);
                    if (entity.Value.GetComponent<Position>().PosY > ScreenCords.Max.Y)
                        Subject.Instance.Notify(Message.YmaxTransition, entity.Key);
                    if (entity.Value.GetComponent<Position>().PosY < 0)
                        Subject.Instance.Notify(Message.YminTransition, entity.Key);
                    if (entity.Value.GetComponent<Position>().PosX < 0)
                        Subject.Instance.Notify(Message.XminTransition, entity.Key);
                }
            }
        }

        public override void OnNotify<T>(Message message, T eventData)
        {
            if (eventData is int)
            {
                var eData = Convert.ToInt32(eventData);
                switch (message)
                {
                    case Message.XmaXTransition:
                        OnXmaXTransition(eData);
                        break;
                    case Message.XminTransition:
                        OnXminTransition(eData);
                        break;
                    case Message.YminTransition:
                        OnYminTransition(eData);
                        break;
                    case Message.YmaxTransition:
                        OnYmaxTransition(eData);
                        break;
                }
            }

        }

        private void OnXmaXTransition(int entityID)
        {
            EntitiesSortedList[entityID].GetComponent<Position>().PosX =
                EntitiesSortedList[entityID].GetComponent<Position>().PosX -
                ScreenCords.Max.X;
        }

        private void OnYmaxTransition(int entityID)
        {
            EntitiesSortedList[entityID].GetComponent<Position>().PosY =
                EntitiesSortedList[entityID].GetComponent<Position>().PosY -
                ScreenCords.Max.Y;
        }

        private void OnXminTransition(int entityID)
        {
            EntitiesSortedList[entityID].GetComponent<Position>().PosX =
                EntitiesSortedList[entityID].GetComponent<Position>().PosX +
                ScreenCords.Max.X;
        }

        private void OnYminTransition(int entityID)
        {
            EntitiesSortedList[entityID].GetComponent<Position>().PosY =
                EntitiesSortedList[entityID].GetComponent<Position>().PosY +
                ScreenCords.Max.Y;
        }
    }

    public class CollisionSubSystem : SubSystem // only collision detection, collision logic goes to logic system (in menu for example collision could trigger different behaviour)
    {
        private List<ColliderPair> triggeredColliders = new List<ColliderPair>();
        public CollisionSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
            : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {

            foreach (var entity in EntitiesSortedList)
            {
                if (entity.Value.GetComponent<BoxCollider>() == null || entity.Value.GetComponent<Position>() == null)
                    continue;
                entity.Value.GetComponent<BoxCollider>().OriginPoint =
                    new Point((int) entity.Value.GetComponent<Position>().PosX - entity.Value.GetComponent<BoxCollider>().Size.X/2,
                        (int) entity.Value.GetComponent<Position>().PosY - entity.Value.GetComponent<BoxCollider>().Size.Y / 2);                      
            }


            triggeredColliders = GetTriggeredColliders() as List<ColliderPair>;

            //find and send data regarding the rocks that've been hit
            triggeredColliders?.FindAll(
                pair =>
                    EntitiesSortedList[pair.DestructorID].GetComponent<LimitedTravelDistance>() != null &&
                    EntitiesSortedList[pair.DestroyedID].GetComponent<DivideAble>() != null)
                .ForEach(p => Subject.Instance.Notify(Message.RockDied, p));
            
            //find and send data on the frags that player perfomed
            triggeredColliders?.FindAll(pair => EntitiesSortedList[pair.DestructorID].GetComponent<Tag>().BindedID == 1)
                .ForEach(p => Subject.Instance.Notify(Message.PlayerFraged, p));

            //tell gamemanager to remove remaing collided entities from the game
            triggeredColliders?.ForEach(pair => Subject.Instance.Notify(Message.EntityKilled, pair.DestroyedID));


        }
        public IEnumerable<ColliderPair> GetTriggeredColliders()
            
        {
            return  from key1 in EntitiesSortedList.Keys
                    from key2 in EntitiesSortedList.Keys
                    let box1 = EntitiesSortedList[key1].GetComponent<BoxCollider>()
                    let box2 = EntitiesSortedList[key2].GetComponent<BoxCollider>()
                    where box1 != null && box2 != null
                    where box1.IsCollidingFilter(box1, box2)
                    where EntitiesSortedList[key2].GetComponent<Tag>().BindedID != key2
                    select new ColliderPair(key1, key2);
        }

        public class ColliderPair
        {
            public int DestructorID { get; }
            public int DestroyedID { get; }

            public ColliderPair(int destructorID, int destroyedID)
            {
                DestructorID = destructorID;
                DestroyedID = destroyedID;
            }           
        }
    }

    public class ManualControlSubSystem : SubSystem
    {
        private readonly Command left = new RotateLeft();
        private readonly Command right = new RotateRight();

        public ManualControlSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
            : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {
            DeltaTime = deltaTime;
        }

        public override void OnNotify<T>(Message message, T eventData)
        {
            if (eventData is int)
            {
                var eventD = Convert.ToInt32(eventData);
                if (EntitiesSortedList.ContainsKey(eventD) &&
                    EntitiesSortedList[eventD].GetComponent<MoveAble>()?.IsActive == true)
                {
                    switch (message)
                    {
                        case Message.Thrust:
                                OnThrust(eventD);
                                break;
                        case Message.RotateRight:
                                OnRotationRight(eventD);
                                break;
                        case Message.RotateLeft:
                                OnRotationLeft(eventD);
                                break;
                        case Message.UfoTurn:
                                OnUfoTurn(eventD);
                                break;
                    }
                }
            }
        }

        private void OnThrust(int entityID)
        {
                EntitiesSortedList[entityID].GetComponent<MoveAble>().VelX +=
                    EntitiesSortedList[entityID].GetComponent<MoveAble>().Acceleration*
                    EntitiesSortedList[entityID].GetComponent<Position>().DirectionX*DeltaTime;
                EntitiesSortedList[entityID].GetComponent<MoveAble>().VelY +=
                    EntitiesSortedList[entityID].GetComponent<MoveAble>().Acceleration*
                    EntitiesSortedList[entityID].GetComponent<Position>().DirectionY*DeltaTime;
        }

        private void OnRotationRight(int entityID)
        {
                EntitiesSortedList[entityID].GetComponent<Position>().RadianOrientation +=
                    EntitiesSortedList[entityID].GetComponent<MoveAble>().RotationSpeed*DeltaTime;

            EntitiesSortedList[entityID].GetComponent<Position>().RadianOrientation =
                MathHelper.WrapAngle((float)EntitiesSortedList[entityID].GetComponent<Position>().RadianOrientation);

        }

        private void OnRotationLeft(int entityID)
        {
                EntitiesSortedList[entityID].GetComponent<Position>().RadianOrientation -=
                    EntitiesSortedList[entityID].GetComponent<MoveAble>().RotationSpeed * DeltaTime;

            EntitiesSortedList[entityID].GetComponent<Position>().RadianOrientation =
            MathHelper.WrapAngle((float)EntitiesSortedList[entityID].GetComponent<Position>().RadianOrientation);
        }

        private void OnUfoTurn(int entityID)
        {
            ServiceLocator.Command.InitCommand(ServiceLocator.Random.CoinFlip() ? left : right, entityID);
            EntitiesSortedList[entityID].GetComponent<MoveAble>().VelX =
                EntitiesSortedList[entityID].GetComponent<Position>().DirectionX * Balance.UfoSpeed;
            EntitiesSortedList[entityID].GetComponent<MoveAble>().VelY =
                EntitiesSortedList[entityID].GetComponent<Position>().DirectionY * Balance.UfoSpeed;
        }
    }

    public class GunControlSubSystem : SubSystem
    {

        public GunControlSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
            : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {
        }

        public override void OnNotify<T>(Message message, T eventData)
        {
            if (eventData is int && EntitiesSortedList.ContainsKey(Convert.ToInt32(eventData)))
            {
                var entityID = Convert.ToInt32(eventData);
                if (EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>() != null)
                {
                    //starting position of entities with limited travel distance needs to be adjusted in case of screen edge transition || muszę to przerzucic do limited travel distance systemu
                    switch (message)
                    {
                        case Message.XmaXTransition:
                            OnXmaXTransition(entityID);
                            break;
                        case Message.XminTransition:
                            OnXminTransition(entityID);
                            break;
                        case Message.YminTransition:
                            OnYminTransition(entityID);
                            break;
                        case Message.YmaxTransition:
                            OnYmaxTransition(entityID);
                            break;
                    }
                }
                if (message == Message.ShotFired)
                {
                    if (EntitiesSortedList[entityID].GetComponent<Gun>() == null)
                        throw new InvalidOperationException("Only entities with gun attached can fire it. Shocking.");

                      OnFire(entityID);
                }
                base.OnNotify(message, eventData);
            }
        }

        private void OnXmaXTransition(int entityID)
        {
            EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>().StartingPosX =
               EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>().StartingPosX -
               ScreenCords.Max.X;
        }

        private void OnXminTransition(int entityID)
        {
            EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>().StartingPosX =
               EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>().StartingPosX +
               ScreenCords.Max.X;
        }

        private void OnYminTransition(int entityID)
        {
            EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>().StartingPosY =
               EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>().StartingPosY +
               ScreenCords.Max.Y;
        }

        private void OnYmaxTransition(int entityID)
        {
            EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>().StartingPosY =
               EntitiesSortedList[entityID].GetComponent<LimitedTravelDistance>().StartingPosY -
               ScreenCords.Max.Y;
        }

        private void OnFire(int entityID)
        {
             Subject.Instance.Notify(Message.EntityAdded, ComponentAssembler.Instance.AssembleBullet(EntitiesSortedList[entityID], entityID));
        }
    }

    public class LimitedPresenceSubSystem : SubSystem
    {
        public LimitedPresenceSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
            : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {
            foreach (var entity in EntitiesSortedList)
            {
                if (entity.Value.GetComponent<LimitedTravelDistance>() != null &&
                    entity.Value.GetComponent<Position>() != null)
                {
                    if (entity.Value.GetComponent<MoveAble>() == null)
                        throw new InvalidOperationException("MoveAble should be attached to a moving object");

                    entity.Value.GetComponent<LimitedTravelDistance>().DistanceTraveled =
                        entity.Value.GetComponent<Position>()
                            .PosVector.Distance(entity.Value.GetComponent<LimitedTravelDistance>().StartingPos);                 

                    if (entity.Value.GetComponent<LimitedTravelDistance>().DistanceTraveled >
                        entity.Value.GetComponent<LimitedTravelDistance>().MaxDistance)
                    {
                        Subject.Instance.Notify(Message.EntityKilled, entity.Key);
                        break;
                    }
                }          
            }
        }
    }

    public class AiSubSystem : SubSystem
    {
        private readonly Dictionary<int, float> shotTimer = new Dictionary<int, float>();
        private readonly Dictionary<int, float> turnTimer = new Dictionary<int, float>();
        private List<int> willShot;


        public AiSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords) : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {
            DeltaTime = deltaTime;
            willShot = new List<int>();
            foreach (var entity in EntitiesSortedList)
            {
                if (entity.Value.GetComponent<AiControlled>() != null &&
                    entity.Value.GetComponent<Gun>() != null &&
                    entity.Value.GetComponent<Position>() != null)
                {

                    if (!shotTimer.ContainsKey(entity.Key))
                        shotTimer.Add(entity.Key, 0);
                    if (!turnTimer.ContainsKey(entity.Key))
                        turnTimer.Add(entity.Key, 0);

                    shotTimer[entity.Key] += deltaTime;
                    turnTimer[entity.Key] += deltaTime;

                    if (shotTimer[entity.Key] > entity.Value.GetComponent<AiControlled>().ShotTick)
                    {
                        if (entity.Value.GetComponent<Gun>().MarksmanshipLevel == 1)
                        {
                           willShot.Add(entity.Key);
                           shotTimer[entity.Key] = 0;
                        }
                    }

                    if (turnTimer[entity.Key] > entity.Value.GetComponent<AiControlled>().DirectionTick)
                    {

                        Subject.Instance.Notify(Message.UfoTurn, entity.Key);
                        turnTimer[entity.Key] = 0;
                    }
                }
                foreach (var _entity in EntitiesSortedList)
                {
                    if (_entity.Value.GetComponent<DivideAble>() != null &&
                        _entity.Value.GetComponent<MoveAble>() != null &&
                        _entity.Value.GetComponent<MoveAble>().IsActive )
                    {
                        ServiceLocator.Command.InitCommand(new RotateLeft(), _entity.Key);
                    }
                }
            }

            willShot.ForEach(id => ServiceLocator.Command.InitCommand(new Fire(), id));      
        }
    }

    public class GameLogicSubSystem : SubSystem
    {
        private float bigUTimer;
        private int scoreLabelID => GetScoreLabelID();
        private int livesPanelID => GetLivesPanelID();

        public GameLogicSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
            : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {
            //TIME for PLAYER to BECOME ACTIVE BOXCOLLIDER (blinking system, after respawn player model will blink and become invunerable for couple of seconds)

            if (EntitiesSortedList.Count == 0)
            {
                Subject.Instance.Notify(Message.EntityAdded, ComponentAssembler.Instance.AssemblePlayer());
                //Subject.Instance.Notify(Message.EntityAdded, ComponentAssembler.Instance.AssembleBigUFO());
            }
            if (IsEnemysRespawnNeeded())
                Subject.Instance.Notify(Message.StartingRocksCreated);
            
            //GuiElements Update
            UpdateScoreLabel();
            UpdateLivesPanel();
                           
        }

        private void UpdateScoreLabel()
        {
            try
            {
                //since it could be easily binded to any other value to update
                var b = EntitiesSortedList[scoreLabelID].GetComponent<Tag>().BindedID;

                EntitiesSortedList[scoreLabelID].GetComponent<Label>().Text =
                    EntitiesSortedList[b].GetComponent<Player>()?.Score.ToString();
            }
            catch (KeyNotFoundException)
            {
                Subject.Instance.Notify(Message.EntityAdded, ComponentAssembler.Instance.AssembleScoreGUILabel());
            }
        
        }

        private void UpdateLivesPanel()
        {
            try
            {
                var b = EntitiesSortedList[scoreLabelID].GetComponent<Tag>().BindedID;
                var score = EntitiesSortedList[b].GetComponent<Player>().Score;

                EntitiesSortedList[scoreLabelID].GetComponent<StateTextures>().AnimState =
                    (AnimState) score;
            }
            catch (KeyNotFoundException)
            {
                Subject.Instance.Notify(Message.EntityAdded, ComponentAssembler.Instance.AssembleLivesGUIPanel());
            }
        }



        private int GetScoreLabelID()
        {
            var scoreLabelID = from key in EntitiesSortedList.Keys
                let e = EntitiesSortedList[key]
                where e.GetComponent<Label>() != null 
                where e.GetComponent<Tag>().Name == "ScoreLabel"  //Tag search only in case of GUI elements              
                select key;

            return scoreLabelID.FirstOrDefault();
        }

        private int GetLivesPanelID()
        {
            var livesPanelID = from key in EntitiesSortedList.Keys
                let e = EntitiesSortedList[key]
                where e.GetComponent<StateTextures>() != null
                where e.GetComponent<Tag>().Name == "LivesPanel"
                select key;

            return livesPanelID.FirstOrDefault();
        }

        private bool IsEnemysRespawnNeeded()
        {
            return EntitiesSortedList.Keys.All(key => EntitiesSortedList[key].GetComponent<ScoreWorth>() == null);
        }

        public override void OnNotify(Message message)
        {
            if (message == Message.ShipDestroyed)
            {
                OnShipDestroyed();
            }
        }

        public override void OnNotify<T>(Message message, T eventData)
        {
            if (eventData is CollisionSubSystem.ColliderPair)
            {
                var eData = eventData as CollisionSubSystem.ColliderPair;
                if (message == Message.RockDied)
                {
                    OnRockDied(eData);
                }

                if (message == Message.PlayerFraged)
                {
                    OnPointsScored(eData.DestroyedID);
                }
            }
        }

        private void OnRockDied(CollisionSubSystem.ColliderPair collPair)
        {
            if (EntitiesSortedList[collPair.DestroyedID].GetComponent<DivideAble>().DivisionInfo == DivideType.Big)
                Subject.Instance.Notify(Message.EntityAdded, collPair, true);
            if (EntitiesSortedList[collPair.DestroyedID].GetComponent<DivideAble>().DivisionInfo == DivideType.Medium)
                Subject.Instance.Notify(Message.EntityAdded, collPair, false);
        }

        private void OnShipDestroyed()
        {
            EntitiesSortedList[1].GetComponent<Player>().Lives -= 1;
            if (EntitiesSortedList[1].GetComponent<Player>().Lives > 0)
                OnPlayerRespawn();
            else
                Subject.Instance.Notify(Message.GameOver);
        }

        private void OnPointsScored(int destroyedEntitysID)
        {
            EntitiesSortedList[1].GetComponent<Player>().Score +=
                        EntitiesSortedList[destroyedEntitysID].GetComponent<ScoreWorth>().PointValue;
        }

        private async void OnPlayerRespawn()
        {
            var player = EntitiesSortedList[1];

            player.GetComponent<Position>().PosX = ScreenCords.PlayerStartingPos.X;
            player.GetComponent<Position>().PosY = ScreenCords.PlayerStartingPos.Y;

            player.GetComponent<BoxCollider>().IsActive = false;
            player.GetComponent<DrawAble>().IsActive = false;
            player.GetComponent<MoveAble>().IsActive = false;


            for (int i = 0; i < 5; i++)
            {
                await Task.Delay(100);
                player.GetComponent<DrawAble>().IsActive = !player.GetComponent<DrawAble>().IsActive;
            }


            player.GetComponent<DrawAble>().IsActive = true;
            player.GetComponent<BoxCollider>().IsActive = true;
            player.GetComponent<MoveAble>().IsActive = true;

        }
    }

    public class StateSubSystem : SubSystem
    {
        public StateSubSystem(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords) : base(entitiesSortedList, screenCords)
        {
        }

        public override void Update(float deltaTime)
        {
            foreach (var entity in EntitiesSortedList)
            {
                var anim = entity.Value.GetComponent<StateTextures>();
                var draw = entity.Value.GetComponent<DrawAble>();
                if (anim != null && draw != null)
                {
                    draw.Texture = anim.Animations[anim.AnimState];
                }
            }
        }

    }

    public class MenuLogic : SubSystem
        {
            public MenuLogic(SortedList<int, List<IComponent>> entitiesSortedList, ScreenCords screenCords)
            : base(entitiesSortedList, screenCords)
            {
            }

            public override void Update(float deltaTime)
            {
                throw new NotImplementedException();
            }
        }
    }





