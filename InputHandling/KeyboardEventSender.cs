﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ECD_Engine.InputHandling
{
    public class KeyboardEventManager : GameComponent
    {
        private readonly List<Keys> pressedKeys = new List<Keys>();
        public List<Keys> PressedKeys => pressedKeys;
        private double intervalP, intervalR;


        public KeyboardEventManager(Game game)
            : base(game)
        { }

        public override void Update(GameTime gameTime)
        {

            intervalP += gameTime.ElapsedGameTime.TotalMilliseconds;
            intervalR += gameTime.ElapsedGameTime.TotalMilliseconds;

            base.Update(gameTime);

            foreach (Keys key in Enum.GetValues(typeof(Keys)))
            {
                if (Keyboard.GetState().IsKeyDown(key) && intervalP > 25)
                {
                    pressedKeys.Add(key);
                    KeyPressed?.Invoke(key);
                    intervalP = 0;
                }
                if (Keyboard.GetState().IsKeyUp(key) && pressedKeys.Contains(key))
                {
                    if (intervalR > 100)
                    {
                        intervalR = 0;
                        KeysRealeased?.Invoke(key);
                    }
                    pressedKeys.Remove(key);

                }
            }
        }

        public event Action<Keys> KeyPressed;
        public event Action<Keys> KeysRealeased;
    }
}

