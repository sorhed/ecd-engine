﻿using System.Threading;
using System.Threading.Tasks;
using ECD_Engine.Patterns;
using Microsoft.Xna.Framework.Input;

namespace ECD_Engine.InputHandling
{
    public interface ICommand
    {
        void InitCommand(Command command, int entityID = 1);
    }

    public class InputHandler : ICommand
    {
        public void HandleInput(Keys key)
        {
            switch (key)
            {
                case Keys.W:
                    InitCommand(new Thrust());
                    break;
                case Keys.D:
                    InitCommand(new RotateRight());
                    break;
                case Keys.A:
                    InitCommand(new RotateLeft());
                    break;
            }
        }

        public void InitCommand(Command command, int entityID = 1)
        {
            command.Execute(entityID);
        }

        public void HandleOnRelease(Keys key)
        {
            if (key == Keys.Space)
                InitCommand(new Fire());
        }
    }
}
    
