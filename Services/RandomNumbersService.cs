﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECD_Engine.Services
{
    public interface IRandom
    {
        bool CoinFlip();
        double RandomDouble(double minVal, double maxVal);
        int RandomInt(int minVal, int maxVal);
    }

    internal class RandomNumbersService : IRandom
    {
        private static readonly Random rand = new Random();

        public bool CoinFlip()
        {
            return rand.Next() % 2 == 0;
        }

        public double RandomDouble(double minVal, double maxVal)
        {
            return rand.NextDouble()*(maxVal - minVal) + minVal;
        }

        public int RandomInt(int minVal, int maxVal)
        {
            return rand.Next(minVal, maxVal);
        }
    }
}
